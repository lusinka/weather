const API_URL = 'http://dataservice.accuweather.com';
const API_KEY = 'TPV0oK7skKVGDSj3szRedNHqJGQsLIrO';

export const getCities = () => {
    const options = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
        },
    };

    return fetch(`${API_URL}/locations/v1/topcities/50?apikey=${API_KEY}`, options).then((res) => {
        if (res.status === 200) {
            return res.json().then(data => ({ ok: true, data }));
        }
        return { ok: false, data: null };
    });
};

export const getWeather = (locationKey) => {
    const options = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
        },
    };
    return fetch(`${API_URL}/forecasts/v1/daily/1day/${locationKey}?apikey=${API_KEY}`, options).then((res) => {
        if (res.status === 200) {
            return res.json().then(weather => ({ ok: true, weather }));
        }
        return { ok: false, weather: null };
    });
};