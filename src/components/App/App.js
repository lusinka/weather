import React, { Component } from 'react';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import orange from '@material-ui/core/colors/orange';
import Root from '../Root/Root';

const theme = createMuiTheme({
  palette: {
    primary: orange,
    secondary: { main: '#11cb5f' },
  },
  typography: {
    fontSize: 16,
    fontFamily: 'Montserrat, sans-serif',
  },
});

class App extends Component {
  render() {
    return (
      <MuiThemeProvider theme={theme}>
        <Root />
      </MuiThemeProvider>
    );
  }
}

export default App;
