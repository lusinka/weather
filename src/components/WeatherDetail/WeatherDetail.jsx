import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import styles from './style'

class WeatherDetail extends Component {

    state = {
        open: false,
    };

    handleClick = (city) => {
        const { doClick } = this.props;
        this.setState({
            open: true,
        });
        doClick(city);
    };

    handleDelete = (city) => {
        const { doRemove } = this.props;
        doRemove(city);
    };

    render() {
        const { classes, selectedCity } = this.props;
        const { open } = this.state;

        return (
            <div className={classes.root}>
                <Table className={classes.table}>
                    <TableHead>
                        <TableRow>
                            <TableCell>
                                CITY
                            </TableCell>
                            <TableCell>{open ? 'DATE' : null}</TableCell>
                            <TableCell>{open ? 'HIGH (°C)' : null}</TableCell>
                            <TableCell>{open ? 'LOW (°C)' : null}</TableCell>
                            <TableCell>
                            </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {selectedCity.map(city => {
                            let maxTemp = ((city.weatherMax - 32) * 5 / 9).toFixed(0);
                            let minTemp = ((city.weatherMin - 32) * 5 / 9).toFixed(0)
                            return (
                                <TableRow key={city['id']}>
                                    <TableCell onClick={() => this.handleClick(city)}>
                                        {city.name}
                                    </TableCell>
                                    <TableCell>
                                        {city['open'] ? new Date().toDateString() : null}
                                    </TableCell>
                                    <TableCell>
                                        {city['open'] ? maxTemp : null}
                                    </TableCell>
                                    <TableCell>
                                        {city['open'] ? minTemp : null}
                                    </TableCell>
                                    <TableCell>
                                        <div className={classes.deleteButton} onClick={() => this.handleDelete(city)}>
                                            <IconButton className={classes.button} aria-label="Delete">
                                                <DeleteIcon color='primary' />
                                            </IconButton>
                                        </div>
                                    </TableCell>
                                </TableRow>
                            )
                        })}
                    </TableBody>
                </Table>
            </div>
        );
    };
};

WeatherDetail.propTypes = {
    classes: PropTypes.object.isRequired,
    selectedCity: PropTypes.array.isRequired,
    doClick: PropTypes.func.isRequired,
    doRemove: PropTypes.func.isRequired,
};

export default withStyles(styles)(WeatherDetail);