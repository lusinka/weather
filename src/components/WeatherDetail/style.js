const styles = theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing.unit * 3,
        overflowX: 'auto',
    },
    table: {
        minWidth: 700,
    },
    deleteButton: {
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'right',
    },
    button: {
        margin: theme.spacing.unit,
    },
});

export default styles;