import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import { getWeather } from '../../fetch';
import styles from './style'

class City extends Component {

    state = {
        error: '',
    };

    handleClick = (key) => {
        const { doClick, city } = this.props;
        getWeather(key).then(({ ok, weather }) => {
            if (ok) {
                let minTemp = '';
                let maxTemp = '';
                weather = weather['DailyForecasts'];
                weather.forEach(el => {
                    maxTemp = el['Temperature']['Maximum']['Value'];
                    minTemp = el['Temperature']['Minimum']['Value'];
                })
                doClick(minTemp, maxTemp, city['EnglishName'])
            };
        }).catch(err => {
            this.setState({
                error: err.toString(),
            });
        });
    };

    render() {
        const { classes, city } = this.props;
        const { error } = this.state;

        return error ? (
            <div className={classes.error}>
                {error}
            </div>
        ) : (
                <ListItem className={classes.nested} onClick={() => this.handleClick(city['Key'])}>
                    <ListItemText inset primary={city['EnglishName']} />
                </ListItem>
            );
    };
};

City.propTypes = {
    classes: PropTypes.object.isRequired,
    city: PropTypes.object.isRequired,
    doClick: PropTypes.func.isRequired,
};

export default withStyles(styles)(City);