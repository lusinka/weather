

const styles = theme => ({
    nested: {
        paddingLeft: theme.spacing.unit * 0,
    },
    root: {
        width: '60%',
        maxWidth: 360,
        backgroundColor: theme.palette.background.paper,
    },
});

export default styles;