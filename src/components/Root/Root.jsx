import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import ListSubheader from '@material-ui/core/ListSubheader';
import List from '@material-ui/core/List';
import Collapse from '@material-ui/core/Collapse';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import { getCities } from '../../fetch';
import City from '../City/City';
import WeatherDetail from '../WeatherDetail/WeatherDetail';
import styles from './style'

class Root extends Component {

    state = {
        open: false,
        cities: [],
        error: '',
        selectedCity: JSON.parse(localStorage.getItem('selectedCity')) || [],
    };

    componentDidMount() {
        getCities().then(({ ok, data }) => {
            if (ok) {
                data = data.slice(0, 10)
                this.setState({
                    cities: data.map(el => ({ ...el, view: true })),
                });
            };
        }).catch(err => {
            this.setState({
                error: err.toString(),
            });
        });
    };

    handleOpen = () => {
        this.setState({
            open: !this.state.open,
        });
    };

    handleWeather = (minTemp, maxTemp, city) => {
        const { selectedCity, cities } = this.state;
        const newCites = cities.map(el => (el['EnglishName'] === city) ? ({ ...el, view: false }) : el)
        this.setState({
            selectedCity: [...selectedCity, { 'name': city, 'id': Date.now(), 'weatherMax': maxTemp, 'weatherMin': minTemp, open: false }],
            cities: newCites,
        });
        this.handleSelectedCities(this.state.selectedCity);
    };

    handleClick = (city) => {
        const { id } = { ...city };
        const { selectedCity } = this.state;
        const newSelectedCity = selectedCity.map(el => (el['id'] === id) ? ({ ...el, open: !el['open'] }) : el);
        this.setState(({ selectedCity }) => ({
            selectedCity: newSelectedCity,
        }));
    };

    handleRemove = (city) => {
        const { selectedCity, cities } = this.state;
        const newCites = cities.map(el => (el['EnglishName'] === city['name']) ? ({ ...el, view: true }) : el);
        const newSelectedCity = selectedCity.filter(el => (el['id'] !== city['id']));
        this.setState(({ selectedCity, cities }) => ({
            selectedCity: newSelectedCity,
            cities: newCites,
        }),
            this.handleSelectedCities(newSelectedCity),
        );
    };

    handleSelectedCities = (data) => {
        const selectedCity = JSON.stringify(data);
        localStorage.setItem('selectedCity', selectedCity);
    };

    render() {
        const { classes } = this.props;
        const { open, cities, error, selectedCity } = this.state;

        return (
            <div className={classes.container}>
                {
                    cities.length ? (
                        <div className={classes.content}>
                            <div className={classes.root}>
                                <List
                                    component="nav"
                                    onClick={this.handleOpen}
                                    subheader={<ListSubheader component="div">
                                        Cities
                                            {open ? <ExpandLess /> : <ExpandMore />}
                                    </ListSubheader>}

                                >
                                    <Collapse in={open} timeout="auto" unmountOnExit>
                                        <List component="div" disablePadding>
                                            {cities.map(city => city['view'] && <City
                                                key={city['Key']} city={city} doClick={this.handleWeather}
                                            />)}
                                        </List>
                                    </Collapse>
                                </List>
                            </div>
                            <div className={classes.weatherDetail}>
                                <WeatherDetail selectedCity={selectedCity} doClick={this.handleClick} doRemove={this.handleRemove} />
                            </div>
                        </div>
                    ) : (
                            error ? (
                                <div className={classes.error}>
                                    {error}
                                </div>
                            ) : (
                                    <div className={classes.containerCircular}>
                                        <div className={classes.circular}>
                                            <CircularProgress color='secondary' size={50} />
                                        </div>
                                    </div>
                                )
                        )
                }
            </div>

        )
    };
}

Root.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Root);