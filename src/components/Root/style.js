

const styles = theme => ({
    container: {
        width: '100%',
        height: '100vh',
        display: 'flex',
        flexDirection: 'column',
        marginTop: 40,
        alignItems: 'center',
    },
    content: {
        width: '100%',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'flex-start',
    },
    root: {
        width: '60%',
        maxWidth: 360,
        backgroundColor: theme.palette.background.paper,
    },
    weatherDetail: {
        width: '100%',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
    },
    error: {
        width: '50%',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
    },
    containerCircular: {
        'min-height': '100vh',
        'overflow-y': 'hidden',
        'overflow-x': 'hidden',
    },
    circular: {
        'margin-top': '300px',
        display: 'flex',
        'justify-content': 'center',
        margin: 'auto',
    },
});

export default styles;